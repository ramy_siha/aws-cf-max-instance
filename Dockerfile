FROM atlassian/pipelines-awscli:1.16.185

RUN  apk update && apk add  jq && wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh

COPY pipe /
COPY pipe.yml /

ENTRYPOINT ["/pipe.sh"]
