#!/usr/bin/env bash
#
# Deploy to AWS S3, http://aws.amazon.com/s3/
#
# Required globals:
#   AWS_ACCESS_KEY_ID
#   AWS_SECRET_ACCESS_KEY
#   AWS_DEFAULT_REGION
#
# Optional globals:
#   CONTENT_ENCODING
#   ACL
#   STORAGE_CLASS
#   CACHE_CONTROL
#   EXPIRES
#   DELETE_FLAG
#   EXTRA_ARGS
#   DEBUG

source "$(dirname "$0")/common.sh"

# mandatory parameters
AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID:?'AWS_ACCESS_KEY_ID variable missing.'}
AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:?'AWS_SECRET_ACCESS_KEY variable missing.'}
AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:?'AWS_DEFAULT_REGION variable missing.'}
STACK_PREFIX=${STACK_PREFIX:?'STACK_PREFIX variable missing.'}


# default parameters
EXTRA_ARGS=${EXTRA_ARGS:=""}

AWS_DEBUG_ARGS=""
if [[ "${DEBUG}" == "true" ]]; then
  info "Enabling debug mode."
  AWS_DEBUG_ARGS="--debug"
fi


declare -A AWS_S3_ARGS=()
AWS_S3_ARGS["cache-control"]=${CACHE_CONTROL}
AWS_S3_ARGS["content-encoding"]=${CONTENT_ENCODING}
AWS_S3_ARGS["acl"]=${ACL}
AWS_S3_ARGS["expires"]=${EXPIRES}
AWS_S3_ARGS["storage-class"]=${STORAGE_CLASS}

info "Starting AWS CloudFormation check..."
set -x

LIST_LENGTH=$(aws cloudformation list-stacks --stack-status-filter CREATE_COMPLETE  | jq --arg STACK_PREFIX $STACK_PREFIX  -c -r '[.StackSummaries[] | select (.StackName | contains($STACK_PREFIX))] | length')
if [ $LIST_LENGTH -gt 3 ]
then
array=( $(aws cloudformation list-stacks --stack-status-filter CREATE_COMPLETE  | jq --arg STACK_PREFIX $STACK_PREFIX  -r  '.StackSummaries[] | select (.StackName | contains($STACK_PREFIX)) | .CreationTime + "@" + .StackName') )
sortedarr=( $(sort  -n < <(printf '%s\n' "${array[@]}")))
COUNTER=1
for line in "${sortedarr[@]}"
do
   stackName=$(echo "$line" | awk -F@ '{print $2}')
   echo "Deleting stack $stackName"
   aws cloudformation delete-stack --stack-name $stackName
   COUNTER=$[$COUNTER +1]
   if [ $COUNTER -gt $(expr $LIST_LENGTH - 3) ]
   then
      break
   fi
done
fi


